use iced::Subscription;
use iced_native::{window, Event};

use crate::message::Message;

pub fn events() -> Subscription<Message> {
    iced_native::subscription::events_with(|event, _status| 
        if let Event::Window(window::Event::CloseRequested) = event {
            Some(Message::Exit)
        }
        else {
            None
        }
    )
}