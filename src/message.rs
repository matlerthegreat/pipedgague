#[derive(Debug, Clone)]
pub enum Message {
    PipedInput(String),
    Exit
}