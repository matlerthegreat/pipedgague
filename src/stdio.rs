use iced_native::subscription::{self, Subscription};
use tokio::io::{stdin, AsyncReadExt};

use crate::message::Message;

type State = String;

pub fn stdio() -> Subscription<Message> {
    struct Stdio;

    subscription::unfold(
        std::any::TypeId::of::<Stdio>(),
        State::default(),
        |state| async {
            let mut buffer = [0; 10];
            stdin().read(&mut buffer[..]).await.expect("stdin read error");
            let piped_input : String = String::from_utf8(buffer.to_vec()).expect("string from_utf8 error");
            (Some(Message::PipedInput(piped_input)), state)
        }
    )
}