mod message;
mod events;
mod stdio;

use iced::{
    Settings,
    pure::{
        Element,
        Application,
        widget::{
            Text,
            Column,
            scrollable::Scrollable
        },
        button,
    },
    Command,
    Subscription,
};

use message::Message;

pub fn main() -> iced::Result {
    Gague::run(Settings::default())
}

#[derive(Debug, Default)]
struct TextLine {
    content: String
}

impl TextLine {
    fn new(s: &str) -> Self {
        TextLine{content: s.to_owned()}
    }
}

#[derive(Debug, Default)]
struct Gague {
    text_lines: Vec<TextLine>,
    should_exit: bool,
}

impl Application for Gague {
    type Message = Message;
    type Executor = iced_futures::backend::native::tokio::Executor;
    type Flags = ();

    fn new(_flags: ()) -> (Gague, Command<Self::Message>) {
        (Gague::default(), Command::none())
    }

    fn title(&self) -> String {
        String::from("PipEd")
    }

    fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
        match message {
            Message::PipedInput(piped_message) => self.text_lines.push(TextLine::new(&piped_message)),
            Message::Exit => {self.text_lines.push(TextLine::new("Exit")); self.should_exit = true},
        }
        
        Command::none()
    }

    fn view(&self) -> Element<Self::Message> {
        let lines = self.text_lines
            .iter()
            .fold(
                Column::new(), 
                |columns, text_line| columns.push(Text::new(&text_line.content)))
                .push(button("Exit").on_press(Message::PipedInput("Click".to_string())));


        Scrollable::new(lines).into()
    }
    
    fn subscription(&self) -> Subscription<Self::Message> {
        Subscription::batch([events::events(), stdio::stdio()])
    }

    fn should_exit(&self) -> bool {
        self.should_exit
    }
}